import React, { useState, useEffect, useCallback } from'react';

function ChatRoom() {
  const [messages, setMessages] = useState([]);
  const [messageText, setMessageText] = useState('');
  // const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    try {
      const storedMessages = sessionStorage.getItem('messages');
      if (storedMessages) {
        setMessages(JSON.parse(storedMessages));
      }
    } catch (error) {
      console.error('Error loading stored messages:', error);
    }
  }, []);

  const handleSendMessage = useCallback(() => {
    if (messageText) {
      const newMessage = {
        text: messageText,
        timestamp: new Date().toLocaleTimeString(),
      };
      setMessages([...messages, newMessage]);
      setMessageText('');
    }
  }, [messageText, messages]);

  useEffect(() => {
    sessionStorage.setItem('messages', JSON.stringify(messages));
  }, [messages]);

  const handleTyping = (e) => {
    setMessageText(e.target.value);
  };

  return (
    <div className="chat-room">
      <h1>Chat Room</h1>
      <form>
        <input
          type="text"
          value={messageText}
          onChange={handleTyping}
          placeholder="Type a message..."
        />
        <button onClick={handleSendMessage}>Send</button>
      </form>
      <div className="message-area">
        {messages.map((message, index) => (
          <p key={index}>
            {message.text} ({message.timestamp})
          </p>
        ))}
      </div>
    </div>
  );
}

export default ChatRoom;